function buscarPelicula() {
    const tituloIngresado = document.getElementById('titulo').value.trim();

    if (tituloIngresado.length < 2) {
        alert('Ingrese el nombre completo de la película.');
        return;
    }

    const titulo = encodeURIComponent(tituloIngresado);

    fetch(`https://www.omdbapi.com/?t=${titulo}&plot=full&apikey=30063268`)
        .then(response => response.json())
        .then(data => {
            if (data.Error || !compararTitulos(data.Title, tituloIngresado)) {
                const resultadoDiv = document.getElementById('resultado');
                resultadoDiv.innerHTML = `<p>No se encontró la película con el nombre exacto.</p>`;
            } else {
                const resultadoDiv = document.getElementById('resultado');
                resultadoDiv.innerHTML = `
                    <h2>${data.Title}</h2>
                    <div>
                        <p><strong>Año de estreno:</strong> ${data.Year}</p>
                        <p><strong>Actores:</strong> ${data.Actors}</p>
                        <p><strong>Reseña:</strong> ${data.Plot}</p>
                        <img src="${data.Poster}" alt="${data.Title} Poster">
                    </div>
                `;
            }
        })
        .catch(error => {
            const resultadoDiv = document.getElementById('resultado');
            resultadoDiv.innerHTML = `<p>Error al obtener los datos: ${error.message}</p>`;
            console.error('Error al obtener los datos:', error);
        });
}

function compararTitulos(tituloApi, tituloIngresado) {
    return tituloApi.trim().toLowerCase() === tituloIngresado.trim().toLowerCase();
}
